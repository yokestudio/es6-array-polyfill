# ES6 Array Polyfill #

A JavaScript polyfill for ES6 Array functions.

Mostly a collection of polyfilled functions retrieved from [MDN](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array).

### Set Up ###

1. Download the latest release (as of 9 Jul 2015, v1.0.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="es6-array-polyfill-1.0.0.min.js"></script>`

### API ###

Refer to [MDN](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array).

### FAQ ###

1. Does this polyfill have any bugs/limitations?

> `Array.prototype[@@iterator]()` is not polyfilled as it only makes sense with Symbols, which is an ES6 feature that cannot be polyfilled correctly and easily.
>
> `Array.from()` only works for array-like objects, and does NOT work with [iterables](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols).
>
> If you find any other bugs/limitations, please let us know.
   
### Contact ###

* Email us at <yokestudio@hotmail.com>.